# CI/CD - Image Scanner

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [CI/CD - Image Scanner](#cicd-image-scanner)
  - [Scopes / Image entries](#scopes-image-entries)
  - [Adding, updating and deleting](#adding-updating-and-deleting)
  - [Trigger a scan](#trigger-a-scan)

<!-- /code_chunk_output -->

Repository for continous container scanning.

## Scopes / Image entries

A `scope` is a set of `image entries`. All `image entries` of a scope are scanned in a scan run. An `image entry` consists of an unique ID, an description and the docker image name.  
The scope name is required when triggering a scan or adding/updating/deleting `image entries`.

## Adding, updating and deleting

To perform a **CUD** (create, update, delete) operation of an **image entry** just trigger a pipeline run in this repo with a variable named `ACTION` set to `update`. Furthermore you have to specify the **target scope** in variable `SCOPE`. If a scope does not exist it is created.

> A deletion of a scope is currently not supported

**Add/Update:**  
Set the variables `ID`, `DESC` and `IMAGE` to your needs. If an entry with the given **id** already exists it's values are updated.

**Delete:**  
Set the variable `ID` but keep `DESC` and `IMAGE` empty/unset

## Trigger a scan

To perform a **scan** of all **image entries** within a **Scope** just trigger a pipeline run in this repo with variable `ACTION` set to `scan`. Furthermore you have to specify the **target scope** in variable `SCOPE`.
