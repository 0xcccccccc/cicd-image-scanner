#!/bin/bash

#--------------------------------------------------------------------------------
# Parameters:
#   #1 - required yes --> Full file path to target JSON file
#   #2 - required yes --> The unique id of the entry to be created/updated/deleted
#   #3 - required no  --> The short description of the image.
#   #4 - required no  --> The image
#
# Hint: To delete an entry provide the unique id of the item but keep description
#       and image values empty.
#--------------------------------------------------------------------------------

JSON_FILE=$1
ID=$2
DESC=$3
IMAGE=$4

if [ -z "${JSON_FILE}" ]; then echo "No JSON file provided"; exit 1; fi
if [ -z "${ID}" ]; then echo "No ID provided"; exit 1; fi

newJSON=
ERR=

if [ -z "${DESC}" ] && [ -z "${IMAGE}" ]
then
    newJSON=$( jq "del(.${ID})" < "${JSON_FILE}")
    ERR=$?
else

    if [ ! -f "${JSON_FILE}" ]
    then
        echo "{}" > "${JSON_FILE}"
    fi

    newJSON=$(jq " .\"${ID}\" = { Desc: \"${DESC}\",Image: \"${IMAGE}\"}" < "${JSON_FILE}")
    ERR=$?
fi

if [ "${ERR}" -eq 0 ]
then
    echo "${newJSON}" > "${JSON_FILE}"
fi

exit ${ERR}