#!/bin/bash

IMAGES_JSON_FILE=
JOB_TEMPLATE=".scanjob.template.gitlab-ci.yml"
PIPELINE_TEMPLATE=".scanner.gitlab-ci.yml"
OUT_FILE="image.scan.gitlab-ci.yml"



#----------------------------------------------------------------------------------------------

function show_help
{
    echo "Usage:"
    echo "----------"
    echo "Available flags:"
    echo "    -h | --help                   show this help message"
    echo "    --images-json <file>          JSON file that contains the images"
    echo "    --job-template <file>         file that contains the template for a scan job"
    echo "    --pipeline-template  <file>   file that contains the basic pipline template"
    echo "    --out <file>                  file where the output is beeing stored"
}

#----------------------------------------------------------------------------------------------

function trace_vars() {
    echo "-----------------------------------------"
    echo "build vars:"
    echo ""
    echo "      Images JSON         : ${IMAGES_JSON_FILE}"
    echo "      Job template        : ${JOB_TEMPLATE}"
    echo "      Pipeline Template   : ${PIPELINE_TEMPLATE}"
    echo "      Out file            : ${OUT_FILE}"
    echo "-----------------------------------------"
}

#----------------------------------------------------------------------------------------------

function makeScanJob 
{
    export SCAN_JOB_NAME="${1}"
    export SCAN_IMAGE="${2}"
    export SCAN_JOB_DESC="${3}"

    envsubst < "${4}" >> "${5}"
}

#----------------------------------------------------------------------------------------------

function main
{
    trace_vars

    if [ -z "$IMAGES_JSON_FILE" ]; then echo "[ERROR] No input file"; exit 1; fi
    if [ ! -f "$IMAGES_JSON_FILE" ]; then echo "[ERROR] Input file '${IMAGES_JSON_FILE}'not found"; exit 1; fi

    cat "${PIPELINE_TEMPLATE}" > "${OUT_FILE}"

    NUM_OF_ELEMS=$( jq ' keys | length' < "${IMAGES_JSON_FILE}")

    for (( c=0; c<"${NUM_OF_ELEMS}"; c++ ))
    do  
        ID=$(jq -r -c "keys | .[$c]" < "${IMAGES_JSON_FILE}")
        DESC=$(jq -r -c .["\"${ID}\""].Desc < "${IMAGES_JSON_FILE}")
        IMAGE=$(jq -r -c .["\"${ID}\""].Image < "${IMAGES_JSON_FILE}")
        makeScanJob "${ID}" "${IMAGE}" "${DESC}" "${JOB_TEMPLATE}" "${OUT_FILE}"
    done

    cat ${OUT_FILE}
}

# parse cmd line flags
while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        --images-json)
        IMAGES_JSON_FILE="$2"
        shift # past argument
        shift # past value
        ;;
        --job-template)
        JOB_TEMPLATE="$2"
        shift # past argument
        shift # past value
        ;;
        --pipeline-template)
        PIPELINE_TEMPLATE="$2"
        shift # past argument
        shift # past value
        ;;
        --out)
        OUT_FILE="$2"
        shift # past argument
        shift # past value
        ;;
        -h|--help)
        show_help
        exit 0
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        echo "Unknown parameter '$key'"
        show_help
        exit 1
        shift # past argument
        ;;
    esac
done

main
